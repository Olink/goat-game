﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FloppyGoat
{
	class ScoreManager
	{
		private int score = 0;
		private SpriteFont font;
		public ScoreManager(SpriteFont f)
		{
			font = f;
		}

		public void AddScore(int val)
		{
			score += val;
		}

		public void Draw(SpriteBatch batch)
		{
			batch.Begin();
			batch.DrawString(font, string.Format("Score: {0}", score), new Vector2(10, 10), Color.Black);
			batch.End();
		}
	}
}
