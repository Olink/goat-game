﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FloppyGoat
{
	class GoatAnimation
	{
		private const int FRAMES = 4;
		private const int CYCLES_PER_FRAME = 5;
		public const int SpriteWidth = 58;
		public const int SpriteHeight = 58;

		private Texture2D spriteSheet;


		private int cycles = 0;
		private int frame = 0;

		public bool shouldAnimate = true;
		public bool shouldMirror = false;

		public GoatAnimation(TextureManager manager)
		{
			spriteSheet = manager.LoadTexture("sprites/goat.png");
		}

		public void Update()
		{
			if (!shouldAnimate)
				return;
			
			cycles++;
			if (cycles > CYCLES_PER_FRAME)
			{
				frame++;
				cycles = 0;
			}

			if (frame >= FRAMES)
				frame = 0;
		}

		private Rectangle GetFrame()
		{
			Rectangle rect = new Rectangle(frame * SpriteWidth, 0, SpriteWidth, SpriteHeight);
			return rect;
		}

		public void Draw(SpriteBatch batch, Vector2 pos)
		{
			batch.Draw(spriteSheet, new Rectangle((int)pos.X, (int)pos.Y, SpriteWidth, SpriteHeight), GetFrame(), Color.White, 0f, Vector2.Zero,
				shouldMirror ? SpriteEffects.FlipHorizontally : SpriteEffects.None, 0f);
		}
	}
}
