﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FloppyGoat
{
	class Poop : Entity
	{
		public Vector2 Position = Vector2.Zero;
		public const int SpriteWidth = 32;
		public const int SpriteHeight = 32;
		private bool dead = false;
		private Texture2D spriteSheet;
		private ScoreManager score;

		public int lifeTime = 400;
		public int cycles = 0;

		public Poop(TextureManager manager, ScoreManager s)
		{
			spriteSheet = manager.LoadTexture("sprites/poop.png");
			score = s;
		}

		public bool isDead()
		{
			return dead;
		}

		public void Update()
		{
			cycles++;

			if (cycles > lifeTime)
			{
				dead = true;
				score.AddScore(1);
			}
		}

		public void Draw(SpriteBatch batch)
		{
			batch.Begin();
			batch.Draw(spriteSheet, Camera.GetScreenPosition(Position), Color.White);
			batch.End();
		}

		public Rectangle getBoundingBox()
		{
			return new Rectangle((int)Position.X, (int)Position.Y, SpriteWidth, SpriteHeight);
		}

		public void Collides(Entity e)
		{
			return;
		}

		public void SetWorld(World w)
		{
			return;
		}

		public void Kill()
		{
			dead = true;
		}
	}
}
