﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FloppyGoat
{
	class Camera
	{
		public static Vector2 Position { get; set; }
		public const int CameraWidth = 640;
		public const int CameraHeight = 480;
		public static Vector2 GetScreenPosition(Vector2 realPosition)
		{
			return new Vector2(realPosition.X - Position.X, realPosition.Y - Position.Y);
		}

		public static bool OnScreen(Rectangle rect)
		{
			Rectangle r = new Rectangle((int)Position.X, (int)Position.Y, CameraWidth, CameraHeight);
			return r.Intersects(rect);
		}

		public static void Move(Vector2 vector)
		{
			Position += vector;
		}
	}
}
