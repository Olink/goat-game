﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FloppyGoat
{
	class TextureManager
	{
		private GraphicsDevice device;
		private string rootPath;

		private Dictionary<String, Texture2D> loadedTextures; 
		public TextureManager(String path, GraphicsDevice device)
		{
			this.device = device;
			rootPath = path;
			loadedTextures = new Dictionary<string, Texture2D>();
		}

		public Texture2D LoadTexture(string file)
		{
			if (loadedTextures.ContainsKey(file))
				return loadedTextures[file];

			string filename = Path.Combine(rootPath, file);
			Texture2D texture = Texture2D.FromStream(device, new FileStream(filename, FileMode.Open, FileAccess.Read));
            Color[] c = new Color[texture.Width * texture.Height];
            texture.GetData(c);
            for( int i = 0; i < c.Length; i++)
            {
                c[i] = Color.FromNonPremultiplied(c[i].ToVector4());
            }
            texture.SetData(c);
            loadedTextures.Add(file, texture);
			return texture;
		}
	}
}
