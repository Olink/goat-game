using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FloppyGoat
{
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class Game1 : Microsoft.Xna.Framework.Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		private TextureManager manager;
		private Goat goat;
		private World w;
		private ScoreManager score;
		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// TODO: Add your initialization logic here

			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);
			manager = new TextureManager("assets", GraphicsDevice);
			score = new ScoreManager(Content.Load<SpriteFont>("arial"));
			goat = new Goat(manager, score);
			w = new World();
			w.AddObject(goat);
			// TODO: use this.Content to load your game content here
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// all content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		private KeyboardState state = Keyboard.GetState();
		private KeyboardState lastState;
		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{

			// Allows the game to exit
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
				this.Exit();

			lastState = state;
			state = Keyboard.GetState();
			if (lastState.IsKeyDown(Keys.Left) && state.IsKeyUp(Keys.Left))
			{
				goat.Velocity -= new Vector2(-2, 0);
			}
			if (lastState.IsKeyDown(Keys.Right) && state.IsKeyUp(Keys.Right))
			{
				goat.Velocity -= new Vector2(2, 0);
			}
			if (lastState.IsKeyDown(Keys.Up) && state.IsKeyUp(Keys.Up))
			{
				goat.Velocity -= new Vector2(0, -2);
			}
			if (lastState.IsKeyDown(Keys.Down) && state.IsKeyUp(Keys.Down))
			{
				goat.Velocity -= new Vector2(0, 2);
			}

			if (state.IsKeyDown(Keys.Left) && lastState.IsKeyUp(Keys.Left))
			{
				goat.Velocity -= new Vector2(2, 0);
			}
			if (state.IsKeyDown(Keys.Right) && lastState.IsKeyUp(Keys.Right))
			{
				goat.Velocity -= new Vector2(-2, 0);
			}
			if (state.IsKeyDown(Keys.Up) && lastState.IsKeyUp(Keys.Up))
			{
				goat.Velocity -= new Vector2(0, 2);
			}
			if (state.IsKeyDown(Keys.Down) && lastState.IsKeyUp(Keys.Down))
			{
				goat.Velocity -= new Vector2(0, -2);
			}

			if (state.IsKeyDown(Keys.W))
			{
				Camera.Move(new Vector2(0, -3));
			}
			if (state.IsKeyDown(Keys.A))
			{
				Camera.Move(new Vector2(-3, 0));
			}
			if (state.IsKeyDown(Keys.S))
			{
				Camera.Move(new Vector2(0, 3));
			}
			if (state.IsKeyDown(Keys.D))
			{
				Camera.Move(new Vector2(3, 0));
			}

			// TODO: Add your update logic here
			w.Update();
			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.CornflowerBlue);

			// TODO: Add your drawing code here
			w.Draw(spriteBatch);
			score.Draw(spriteBatch);
			base.Draw(gameTime);
		}
	}
}
