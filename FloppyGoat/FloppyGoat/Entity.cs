﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FloppyGoat
{
	interface Entity
	{
		bool isDead();
		void Update();
		void Draw(SpriteBatch batch);
		Rectangle getBoundingBox();
		void Collides(Entity e);
		void SetWorld(World w);
		void Kill();
	}
}
