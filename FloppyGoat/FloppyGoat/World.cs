﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework.Graphics;

namespace FloppyGoat
{
	class World
	{
		private List<Entity> objects;

		public World()
		{
			objects = new List<Entity>();
		}

		public void Update()
		{
			for (int i = objects.Count - 1; i >= 0; --i)
			{
				if (!Camera.OnScreen(objects[i].getBoundingBox()))
					objects[i].Kill();

				if (objects[i].isDead())
				{
					objects.RemoveAt(i);
					continue;
				}

				objects[i].Update();
			}


			CheckCollision();
		}

		private void CheckCollision()
		{
			foreach (Entity e in objects)
			{
				List<Entity> notChecked = new List<Entity>(objects);
				notChecked.Remove(e);

				foreach (Entity e2 in notChecked)
				{
					e.Collides(e2);
				}
			}
		}

		public void Draw(SpriteBatch batch)
		{
			foreach (Entity e in objects)
			{
				e.Draw(batch);
			}
		}

		public void AddObject(Entity e)
		{
			e.SetWorld(this);
			objects.Add(e);
		}
	}
}
