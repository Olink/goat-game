﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FloppyGoat
{
	class Goat : Entity
	{
		private Vector2 Position = Vector2.Zero;
		public Vector2 Velocity = Vector2.Zero;
		private GoatAnimation animation;
		private bool dead = false;
		private World world;
		private TextureManager manager;
		private ScoreManager score;
		public Goat(TextureManager m, ScoreManager s)
		{
			manager = m;
			animation = new GoatAnimation(manager);
			score = s;
		}

		public void SetWorld(World w)
		{
			world = w;
		}

		public void Kill()
		{
			return;
		}

		public bool isDead()
		{
			return dead;
		}

		public void Update()
		{
			animation.Update();
			Position += Velocity;

			if (Velocity.X > 0 && animation.shouldMirror)
				animation.shouldMirror = false;
			else if (Velocity.X < 0 && !animation.shouldMirror)
				animation.shouldMirror = true;

			if (Velocity == Vector2.Zero)
				animation.shouldAnimate = false;
			else
				animation.shouldAnimate = true;

			/*Random r = new Random();
			if (r.Next(10000) < 2000)
			{
				Poop p = new Poop(manager, score);
				if (animation.shouldMirror)
					p.Position = Position + new Vector2(GoatAnimation.SpriteWidth + 5, GoatAnimation.SpriteHeight - Poop.SpriteHeight);
				else
					p.Position = Position + new Vector2(-(Poop.SpriteWidth + 5), GoatAnimation.SpriteHeight - Poop.SpriteHeight);

				world.AddObject(p);
			}*/
		}

		public void Draw(SpriteBatch batch)
		{
			batch.Begin();
			animation.Draw(batch, Camera.GetScreenPosition(Position));
			batch.End();
		}

		public Rectangle getBoundingBox()
		{
			return new Rectangle((int)Position.X, (int)Position.Y, GoatAnimation.SpriteWidth, GoatAnimation.SpriteHeight);
		}

		public void Collides(Entity e)
		{
			if (e.getBoundingBox().Intersects(getBoundingBox()))
			{
				if(e.GetType() == typeof(Poop))
				{
					e.Kill();
					score.AddScore(-1);
				}
			}
		}
	}
}
